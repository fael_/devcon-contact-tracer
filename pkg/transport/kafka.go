package transport

import (
	"context"
	"io"

	"github.com/segmentio/kafka-go"
	log "github.com/sirupsen/logrus"
)

const (
	DefaultMinBytes = 10e3
	DefaultMaxBytes = 10e6
)

type Kafka struct {
	kafka.WriterConfig
	kafka.ReaderConfig
}

type KafkaConfig struct {
	Brokers   []string
	Topic     string
	Partition int
	MinBytes  int
	MaxBytes  int
}

func NewKafkaConfig(brokers []string, minBytes, maxBytes int) *KafkaConfig {
	return &KafkaConfig{
		Brokers:  brokers,
		MinBytes: minBytes,
		MaxBytes: maxBytes,
	}
}

func NewKafka(config *KafkaConfig) MessageBus {
	if config.MinBytes == 0 {
		config.MinBytes = DefaultMinBytes
	}
	if config.MaxBytes == 0 {
		config.MaxBytes = DefaultMaxBytes
	}
	wconf := kafka.WriterConfig{
		Brokers:  config.Brokers,
		Topic:    config.Topic,
		Balancer: &kafka.LeastBytes{},
	}
	rconf := kafka.ReaderConfig{
		Brokers:   config.Brokers,
		Topic:     config.Topic,
		Partition: config.Partition,
		MinBytes:  config.MinBytes,
		MaxBytes:  config.MaxBytes,
	}
	return &Kafka{
		WriterConfig: wconf,
		ReaderConfig: rconf,
	}
}

func (k *Kafka) Publish(topic string, msgs ...Message) error {
	config := k.WriterConfig
	config.Topic = topic
	writer := kafka.NewWriter(config)
	defer writer.Close()
	messages := make([]kafka.Message, len(msgs))
	for i, msg := range msgs {
		messages[i] = toKafkaMessage(msg)
	}
	log.WithField("channel", topic).WithField("messages", len(messages)).Info("Publishing messages")
	return writer.WriteMessages(context.Background(), messages...)
}

func (k *Kafka) Subscribe(groupID, topic string, callback OnMessageReceived) (io.Closer, error) {
	log.WithField("topic", topic).WithField("groupID", groupID).Info("Subscribed to channel")
	config := k.ReaderConfig
	config.Topic = topic
	config.GroupID = groupID
	reader := kafka.NewReader(config)
	go func() {
		for {
			message, err := reader.ReadMessage(context.Background())
			if err != nil {
				log.WithError(err).WithField("channel", topic).Error("error when reading channel")
				if err := reader.Close(); err != nil {
					log.WithError(err).WithField("channel", topic).Warn("error closing connection. might already be closed")
				}
				break
			}
			msg := fromKafkaMessage(message)
			callback(msg)
		}
	}()
	return reader, nil
}

func toKafkaMessage(msg Message) kafka.Message {
	return kafka.Message{
		Key:   msg.Key,
		Value: msg.Value,
	}
}

func fromKafkaMessage(message kafka.Message) *Message {
	return &Message{
		Key:   message.Key,
		Value: message.Value,
	}
}
