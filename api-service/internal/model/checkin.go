package model

import (
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model/errors"
)

type CheckInType string

const (
	CheckInTypeLatLong = "coordinates"
	CheckInTypePOI     = "poi"
)

type CheckInRequest struct {
	CheckInType CheckInType `json:"checkInType"`
	Lat         float64     `json:"lat,omitempty"`
	Long        float64     `json:"long,omitempty"`
	POIID       string      `json:"poiId,omitempty"`
}

func (c *CheckInRequest) Validate() errors.JSONErrors {
	switch c.CheckInType {
	case CheckInTypeLatLong:
		// no checking. 0,0 is valid coordinates
		return nil
	case CheckInTypePOI:
		if c.POIID == "" {
			return errors.New().
				Add("400", map[string]string{"pointer": "/data/poiId"}, "Invalid poiId", "poiId should not be empty")
		}
	default:
		// type is required
		return errors.New().
			Add("400",
				map[string]string{"pointer": "/data/checkInType"},
				"Invalid checkInType",
				"checkInType is invalid. Valid types are: coordinates, poi")
	}
	return nil
}

func (c *CheckInRequest) Coordinates() (lat, long float64) {
	return c.Lat, c.Long
}

func (c *CheckInRequest) POI() string {
	return c.POIID
}

type CheckInResponse struct {
	RequestUUID string `json:"requestUuid,required"`
}
