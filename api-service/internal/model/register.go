package model

type Register struct {
	MobileNumber string `json:"mobileNo,omitempty"`
	Email        string `json:"email,omitempty"`
	Password     string `json:"password,omitempty"`
}
