package handler

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/service"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/utils"
)

type LocationHandler struct {
	*service.LocationService
}

func NewLocationHandler(l *service.LocationService) *LocationHandler {
	handler := &LocationHandler{LocationService: l}
	return handler
}

func (h *LocationHandler) Register(router *mux.Router) {
	locRoute := router.PathPrefix("/loc")

	locRoute.
		Methods(http.MethodPost, http.MethodOptions).
		Path("/check-in").
		Handler(checkin(h.LocationService))
	log.Info("[POST] /loc/check-in registered")
}

func checkin(locationService *service.LocationService) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		var data model.CheckInRequest
		if err := utils.ReadEntity(req, &data); err != nil {
			utils.WriteError(res, http.StatusBadRequest, err)
			return
		}

		if err := data.Validate(); err != nil {
			utils.WriteError(res, http.StatusBadRequest, err)
			return
		}

		checkinResponse, err := locationService.Checkin(&data)
		if err != nil {
			utils.WriteError(res, http.StatusInternalServerError, err)
			return
		}

		utils.WriteEntity(res, http.StatusOK, checkinResponse)
	}

}
