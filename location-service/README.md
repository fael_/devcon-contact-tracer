location-service
----------------

The location service handles all location related transaction such as checkin, etc. 

## Getting Started

To clone the repo:

```shell
git clone git@gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git
cd location-service
```

A docker image is also avaible in Docker Hub:

```shell
docker pull dctx/location-service:latest
```

## Prerequisire

### Workspace

- Go
- Docker

### Docker images

- Kafka (bitnami/kafka:2.4.1)

### Deployment

- Docker Compose
- Kubernetes (minikube)
- Helm

## Build

Run the following to build the binary:

```shell
make build
```

The binary will be located in: `build/bin/location-service`

## Configuration

Configuration is handled by [viper](https://github.com/spf13/viper) which allows configuration using a config file or by environment variables.

A sample configuration file can be found in `config/.location-service.yaml`. Copy this to the `$HOME` directory to override the defaults.

Setting environment variables can also override the default configuration:

| Environment Variable             | Description                                      | Default                   |
| -------------------------------- | ------------------------------------------------ | ------------------------- |
| LOCATION_SERVICE_SERVER_HOST     | Name of the host or interface to bind the server | 0.0.0.0                   |
| LOCATION_SERVICE_SERVER_PORT     | Port to bind the server                          | 8080                      |
| LOCATION_SERVICE_KAFKA_BROKERS   | Addresses of the Kafka Brokers                   | broker1:9092,broker2:9092 |
| LOCATION_SERVICE_KAFKA_TOPIC     | The topic to publish/subscribe to                | dctx-commands             |
| LOCATION_SERVICE_KAFKA_PARTITION | The topic partition                              | 0                         |
| LOCATION_SERVICE_KAFKA_MINBYTES  | Min Bytes when reading messages                  | 10000                     |
| LOCATION_SERVICE_KAFKA_MAXBYTES  | Max Bytes when reading messages                  | 10000000                  |

## Starting the Service

```shell
make run
```

## Packaging Image

To create the docker image:

```
make package
```

To publish the image to docker hub:

```
make publish
```

Note that publishing the image requires access to the dctx group in docker hub.

## Development

This project follows gitlab flow. The general flow is:

- Pick / Create an issue
- Create a feature branch
- Open a Merge Request
- Maintainer merge's to `master`

Read more about gitlab flow [here](https://docs.gitlab.com/ee/topics/gitlab_flow.html).

### Project Structure

```
location-service
|- build/               # build artifacts are generated here
|- cmd/                 # command line commands live here. Checkout cobra library
|- config/              # configuration files are here
|- helm/                # helm chart for kubernetes deployment
|- internal/            # for internal go packages 
| |- server
| |- ...
|- pkg/                 # for public go packages
|- .dockerignore        # ignore list for docker
|- .gitignore           # ignore list for git
|- go.mod               # dependencies for project
|- go.sum               # checksum for dependencies, do not manually change
|- main.go              # the main go file
|- Makefile             # build scripts
|- README.md            # this file
```

### Adding Dependencies

To add dependencies, run the following:

```shell
go get -u {dependency}
make deps
```

## TODO

Initial ToDo list:

- [ ] Improve this README
- [ ] Setup Gitlab CI/CD
- [ ] Create kafka connector
- [ ] Define features in Gitlab Issues or Trello