package service

import (
	"github.com/prometheus/common/log"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport"
)

const (
	CommandChannel = "dctx.contact-tracer.api"
	GroupID        = "location-service"
)

type CheckInService struct {
	transport.MessageBus
}

func NewCheckInService(mb transport.MessageBus) *CheckInService {
	return &CheckInService{mb}
}

func (s *CheckInService) Start() {
	s.Subscribe(GroupID, CommandChannel, processCheckIns)
}

func processCheckIns(msg *transport.Message) {
	log.Info("message received")
}
