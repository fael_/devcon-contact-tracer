clean: stop
	@echo 'cleaning volumes'
	@rm -r mount | true

run:
	@echo 'running services'
	@docker-compose build
	@docker-compose up -d

stop:
	@echo 'stopping services'
	@docker-compose down